//Dependencies
const User = require("../models/User");
const bcrypt = require("bcryptjs");

const auth = require("../auth");

const Course = require("../models/Course");

//REGISTER USER
module.exports.registerUser = (req, res) => {
    console.log(req.body);
    //syntax of bcrypt:
    //bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
    //saltrounds - no. of time to randomize the char
    //
    req.body;
    let passwordInput = req.body.password;

    const hashedPW = bcrypt.hashSync(passwordInput, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPW,
        mobileNumber: req.body.mobileNumber,
    });
    newUser
        .save()
        .then((user) => res.send(user))
        .catch((err) => res.send(err));
};

//retrieve All Users
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//login user

module.exports.loginUser = (req, res) => {
    console.log(req.body);
    /* 
                                                                                                                                                                1. Find the user by the email
                                                                                                                                                                2. If found, we will check the password
                                                                                                                                                                3. If not found, we will send a message to the client. 
                                                                                                                                                                4. If user's input password is same as our stored password, we will generate token/key to access the app. If not, we will turn them away with a message. 
                                                                                                                                                            */

    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send("User not found in the database.");
            } else {
                //comparing of password
                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );
                console.log(isPasswordCorrect);

                /* compareSync()
will return a boolean value if it match=true, not match=false */
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(foundUser) });
                } else {
                    return res.send("Incorrect Password, please try again!");
                }
            }
        })
        .catch((err) => res.send(err));
};

//get single user details
module.exports.getUserDetails = (req, res) => {
    console.log(req.user);

    User.findById(req.user.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//check if email exists
module.exports.checkEmailControllers = (req, res) => {
    User.findOne({ email: req.body.email })
        .then((result) => {
            if (result !== null && result.email === req.body.email) {
                return res.send("Email is already registered!");
            } else {
                return res.send("Email is available");
            }
        })
        .catch((error) => res.send(error));
};

// Updating User Detail by anyone

module.exports.updateUserDetails = (req, res) => {
    console.log("Input for new value: " + req.body);
    console.log("Logged In's User ID: " + req.user.id);

    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNumber: req.body.mobileNumber,
    };
    User.findByIdAndUpdate(req.user.id, updates, { new: true })
        .then((updatedUserInfo) => res.send(updatedUserInfo))
        .catch((err) => res.send(err));
};

//update an ADMIN

module.exports.updateAdmin = (req, res) => {
    console.log("User ID (logged in): " + req.user.id);
    console.log("ID of the user to update: " + req.params.id);

    let updates = {
        isAdmin: req.body.isAdmin,
    };
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedAdmin) => res.send(updatedAdmin))
        .catch((err) => res.send(err));
};

//enrollment of user

//async makes the function asynchronous (it will wait for the result of the function)
//await should always have async

module.exports.enrollment = async(req, res) => {
    /* 
                                                                1. look for the user by its ID
                                                                    -- push the details of the course trying to enroll in. Push it to a new sub document in our User.
                                                                2. look for the course by its ID
                                                                    -- push the details of the users/enrollee. Push it to a new enrollees sub document in Course.
                                                                3. Send a message to the client when both documents are saved successfully.
                                                                */
    console.log("HELLO");
    console.log(req.user.id); // the user id from the decoded token after verification
    console.log(req.body.courseId); //course will be taken from req body
    console.log(req); //

    // limit admin feature to prevent them from enrolling

    if (req.user.isAdmin) {
        return res.send("Your account is an Admin. Action Forbidden");
    }

    let isUserUpdated = await User.findById(req.user.id).then((user) => {
        console.log(user);
        let newEnrollment = {
            courseId: req.body.courseId,
        };
        user.enrollments.push(newEnrollment);
        return user
            .save()
            .then((user) => true)
            .catch((err) => err.message);
    });

    if (isUserUpdated != true) {
        return res.send({ message: isUserUpdated });
    }

    let isCourseUpdated = await Course.findById(req.body.courseId).then(
        (course) => {
            console.log(course);
            let enrollee = {
                userId: req.user.id,
            };
            course.enrollees.push(enrollee);
            return course
                .save()
                .then((course) => true)
                .catch((err) => err.message);
        }
    );
    if (isCourseUpdated != true) {
        return res.send({ message: isCourseUpdated });
    }
    if (isUserUpdated && isCourseUpdated) {
        return res.send({
            message: "User enrolled successfully",
        });
    }
};

//Get Enrollments - user can see all his enrolled course.

module.exports.getEnrollments = (req, res) => {
    User.findById(req.user.id)
        .then((result) => res.send(result.enrollments))
        .catch((err) => res.send(err));
};