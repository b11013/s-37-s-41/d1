const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Course name is required"],
    },

    description: {
        type: String,
        required: [true, "Course description is required"],
    },

    price: {
        type: Number,
        required: [true, "Course name is required"],
    },

    isActive: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
    //embedded doc:
    enrollees: [{
        userId: { type: String, required: [true, "User ID is required."] },
        status: { type: String, default: "Enrolled" },
        dateEnrolled: { type: Date, default: new Date() },
    }, ],
});
//capitalized & singular forms
module.exports = mongoose.model("Course", courseSchema);